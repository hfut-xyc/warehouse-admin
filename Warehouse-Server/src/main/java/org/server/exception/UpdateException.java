package org.server.exception;

public class UpdateException extends RuntimeException {

	public UpdateException(String message) {
		super(message);
	}
}
