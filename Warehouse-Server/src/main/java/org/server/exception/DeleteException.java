package org.server.exception;

public class DeleteException extends RuntimeException {

	public DeleteException(String message) {
		super(message);
	}
}
